$(window).load(function () {
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
        $('body').addClass('ios');
    } else {
        $('body').addClass('web');
    }

    $('body').removeClass('loaded');
});

/* viewport width */
function viewport() {
    var e = window,
        a = 'inner';
    if (!( 'innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']}
}
/* viewport width */

$(function () {
    /* placeholder*/
    /*$('input, textarea').each(function () {
     var placeholder = $(this).attr('placeholder');
     $(this).focus(function () {
     $(this).attr('placeholder', '');
     });
     $(this).focusout(function () {
     $(this).attr('placeholder', placeholder);
     });
     });*/
    /* placeholder*/

    $('.button-nav').click(function () {
        $(this).toggleClass('active'),
            $('.main-nav-list').slideToggle();
        return false;
    });

    /* components */

    /*
     if ($('.fancybox').length) {
     $('.fancybox').fancybox({
     margon: 10,
     padding: 10
     });
     };

     if ($('.scroll').length) {
     $(".scroll").mCustomScrollbar({
     axis: "x",
     theme: "dark-thin",
     autoExpandScrollbar: true,
     advanced: {autoExpandHorizontalScroll: true}
     });
     };
     */

    /* components */

    if ($('.main-slider').length) {
        // $('.main-slider').slick({
        //     arrow: true,
        //     dots: false,
        //     infinite: true,
        //     speed: 300,
        //     slidesToShow: 1,
        //     slidesToScroll: 1,
        //     fade: true,
        //     speed: 1000,
        //     responsive: [
        //         {
        //             breakpoint: 1024,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 3,
        //                 infinite: true,
        //                 dots: true
        //             }
        //         },
        //         {
        //             breakpoint: 600,
        //             settings: "unslick"
        //         }
        //     ]
        // });
        $('.main-slider__slider').owlCarousel({
            loop:true,
            margin:10,
            nav:true,
            items: 1
        });
    }

    if ($('.categories-tab').length) {
        $('.categories-tab').tabs();
    }

    if ($('.promotion-slider').length) {
        $('.promotion-slider').slick({
            autoplay: true,
            dots: true,
            arrows: false,
            slidesToShow: 2,
            responsive: [{
                        breakpoint: 650,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }]
                    
        });
    }
});



/*  main menu */
function menu_opened() {
    var menu_item = $('.categories-nav__item');
    var submenu_wrap = $('.categories-nav__submenu-wrap');

    submenu_wrap.parent().find('.categories-nav__link').prepend('<i class="fa fa-chevron-down hidden-max1200" aria-hidden="true"></i>');

    menu_item.hover(function () {
        $(this).find('.categories-nav__submenu-wrap').addClass('submenu-opened');
        $(this).find('.fa-chevron-down').addClass('menu-opened');
    }, function () {
        $(this).find('.categories-nav__submenu-wrap').removeClass('submenu-opened');
        $(this).find('.fa-chevron-down').removeClass('menu-opened');
    });
}

function menu_opened_mobile() {
    var menu_item = $('.categories-nav__item');
    var submenu_wrap = $('.categories-nav__submenu-wrap');

    submenu_wrap.parent().find('.categories-nav__link').prepend('<i class="fa fa-chevron-down hidden-1200" aria-hidden="true"></i>');

    menu_item.on('click', function (e) {
        e.preventDefault();
        $(this).find('.categories-nav__submenu-wrap').toggleClass('submenu-opened');
        $(this).find('.fa-chevron-down').toggleClass('menu-opened');
    });
}

if ($(window).width() >= 1024) {
    menu_opened();
} else {
    menu_opened_mobile();
}

$(document).resize(function () {
    if ($(window).width() >= 1024) {
        menu_opened();
    } else {
        menu_opened_mobile();
    }
});

$(document).scroll(function () {
    var header = $('.header'),
        menu_top = $('.header-top'),
        menu_bottom = $('.header-bottom');
        // console.log($(document).scrollTop(), header.height(), menu_bottom.height());
    if ($(document).scrollTop() >= header.height() - menu_bottom.height()) {
        menu_bottom.addClass('menu-fixed');
        menu_top.css('padding-bottom', menu_bottom.height());
        // console.log($(window).width());
        // if($(window).width() < 992){
        //     $('.content').css('padding-top', 90);
        //
        // }

    } else {
        menu_bottom.removeClass('menu-fixed');
        menu_top.css('padding-bottom', 18);
        // if($(window).width > 992){
        //     $('.content').css('padding-top', 0);
        // }
    }
});

/* to-top */
$(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('#to-top').addClass('visible');
        } else {
            $('#to-top').removeClass('visible');
        }
    });

    $('#to-top').on('click', function (e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0
        }, 800);
    });
});

/* styled form */
if ($('.styled').length) {
    $('.styled').styler();
}

/* search */
$('.header-button.button-search .fa-2').on('click', function (e) {
    e.preventDefault();
    $('.header-search').toggleClass('search-visible');
    $(this).addClass('fa-notactive');
});

$('#header_search').on('blur', function () {
    $('.header-search').removeClass('search-visible');
    $('.header-button.button-search .fa-2').removeClass('fa-notactive');
});

$('.header-button.button-search .fa-2').mouseenter(function () {
    $(this).addClass('fa-hover');
});
$('.header-button.button-search .fa-2').mouseleave(function () {
    $(this).removeClass('fa-hover');
});
/* switch display product list */
var switch_item = $('.catalog-block__item-switch');

switch_item.on('click', function () {
    switch_item.removeClass('active');
    $(this).addClass('active');
});

/* price slider */
var pricesSlider = $("#prices-slider"),
    minCost = $("#min-cost"),
    maxCost = $("#max-cost");

pricesSlider.slider({
    min: 0,
    max: 99999,
    values: [967, 27164],
    range: true,
    stop: function (event, ui) {
        minCost.val(pricesSlider.slider("values", 0));
        maxCost.val(pricesSlider.slider("values", 1));
    },
    slide: function (event, ui) {
        minCost.val(pricesSlider.slider("values", 0));
        maxCost.val(pricesSlider.slider("values", 1));
    }
});

minCost.change(function () {
    var value1 = minCost.val();
    var value2 = maxCost.val();

    if (parseInt(value1) > parseInt(value2)) {
        value1 = value2;
        minCost.val(value1);
    }
    pricesSlider.slider("values", 0, value1);
});

maxCost.change(function () {
    var value1 = minCost.val();
    var value2 = maxCost.val();

    if (value2 > 1000) {
        value2 = 1000;
        maxCost.val(1000)
    }

    if (parseInt(value1) > parseInt(value2)) {
        value2 = value1;
        maxCost.val(value2);
    }
    pricesSlider.slider("values", 1, value2);
});

/* filter section switch */
$('.filter-block__title').on('click', function () {
    $(this).children().toggleClass('arrow-rotate');
    $(this).next().slideToggle(250);
});
$('.filter-block__feed').on('click', function () {
    $(this).children().toggleClass('arrow-rotate');
    $(this).next().slideToggle(250);
});

/* product line rating */
if ($('.product-block__rating').length > 0) {
    $('.product-block__rating').raty({
        half: true,
        score: 4.5
    });
}

var handler = function () {

    var height_footer = $('footer').height();
    var height_header = $('header').height();
    //$('.content').css({'padding-bottom':height_footer+40, 'padding-top':height_header+40});


    var viewport_wid = viewport().width;
    var viewport_height = viewport().height;

    if (viewport_wid <= 991) {

    }
}

$(window).bind('load', handler);
$(window).bind('resize', handler);



$(document).ready(function () {
    // burger
    $('.mobile-burger').click(function () {
        $(this).toggleClass('open');
        $('body').toggleClass('noscroll');
        $('.mobile-menu').toggleClass('opened');
        $('.overlay').fadeToggle();
    });

    $('.overlay').click(function () {
        $(this).fadeOut();
        $('.mobile-menu').removeClass('opened');
        $('body').removeClass('noscroll');
        $('.mobile-burger').removeClass('open');
    });
    // mobile menu

    $('.navigtaion-list__item a').click(function (e) {
        if($(this).parents('.navigtaion-list__item').hasClass('submenu-link')){
            e.preventDefault();
            var target = $(this).parents('.navigtaion-list__item').data('target');
            $('.mobile-menu__second[data-target=' + target +']').toggleClass('opened');
            $('.navigtaion-list__item[data-target=' + target +']').toggleClass('active');
        }
    });

    // navigation btn click
    $('.navigation-btn').click(function () {
        var slider = $(this).data('slider-navigation');
        var direction = $(this).data('owl-navigaion');
        $('.' + slider + ' .owl-' + direction + '').trigger('click');
    });

    //show more
    $('.show-all').click(function () {
        $(this).prev().toggleClass('fullHeight');
    });
    var get_tab = 1;
    $('.mobile-tabs-pagination .pagination-right').click(function () {
        // var get_tab = parseInt($(this).data('tab'));
        var tab_length = $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item').length;
        $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').removeClass('visible-tab');
        if(get_tab < tab_length){
            get_tab++;
            $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').addClass('visible-tab');
            $('.visible-tab a').trigger('click');
        }
        else{
            get_tab = 1;
            $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').addClass('visible-tab');
            $('.visible-tab a').trigger('click');
        }
    });
    $('.mobile-tabs-pagination .pagination-left').click(function () {
        var tab_length = $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item').length;
        $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').removeClass('visible-tab');
        if (get_tab > 1){
            get_tab--;
            $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').addClass('visible-tab');
            $('.visible-tab a').trigger('click');
        }
        else{
            get_tab = tab_length;
            $(this).parents('.categories-tab').children('.categories-tab__tabs-block').children('.categories-tab__item[data-tab=' + get_tab + ']').addClass('visible-tab');
            $('.visible-tab a').trigger('click');
        }
    });

});
